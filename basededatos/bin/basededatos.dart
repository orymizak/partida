import 'jugador.dart';
import 'partida.dart';
import 'repositorio_memoria.dart';

void main(List<String> arguments) {
  RepositorioMemoria r = RepositorioMemoria();

  Jugador juan = Jugador('Juan');
  print('existe juan?');
  print(r.registradoJugador(juan));
  r.registrarJugador(juan);
  print(r.registradoJugador(juan));

  Partida p = Partida(
      jugador: [juan],
      cartasAzules: 4,
      cartasVerdes: 4,
      cartasRosas: 2,
      cartasGrises: 1);
  r.registrarPartida(p, juan);
  r.registrarPartida(p, juan);
  r.recuperarPartidas(juan);

  List<Partida> l = r.recuperarPartidas(juan);
  print(l);
}
