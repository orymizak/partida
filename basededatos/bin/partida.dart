import 'jugador.dart';

class Partida {
  final List<Jugador> jugador;
  final int cartasAzules;
  final int cartasVerdes;
  final int cartasRosas;
  final int cartasGrises;

  Partida({required this.jugador, required this.cartasAzules, required this.cartasVerdes, required this.cartasRosas, required this.cartasGrises});
}
