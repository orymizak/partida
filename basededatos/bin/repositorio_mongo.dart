import 'partida.dart';
import 'jugador.dart';
import 'repositorio_ideal.dart';
import 'package:mongo_dart/mongo_dart.dart';

class RepositorioMongo extends RepositorioIdeal {
  late Db _datos;

  RepositorioMongo() {
    inicializar();

  }

  void inicializar() async {
    _datos = await Db.create(
        'mongodb+srv://orymizak:993380@cluster0.hdhsd.mongodb.net/test?retryWrites=true&w=majority');
    await _datos.open();
    
  }

  @override
  List<Partida> recuperarPartidas(Jugador j) {
    throw UnimplementedError();
  }

  @override
  bool registradoJugador(Jugador j) {

  return true;

  }

  @override
  bool registrarJugador(Jugador j) {
    throw UnimplementedError();
  }

  @override
  void registrarPartida(Partida p, Jugador j) {}
}
