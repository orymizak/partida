import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<charts.Series<Partida, String>> _seriesData = [];
  var puntosPorRosa = [
  0,
  1,
  3,
  6,
  10,
  15,
  21,
  28,
  36,
  45,
  55,
  66,
  78,
  91,
  105,
  120,
];

  _generateData() {
    var ronda1 = [
      Partida(Jugador: "Andres", cartasAzules: 3, cartasVerdes: 0, cartasRosas: 0, cartasGrises: 0),
      Partida(Jugador: "Marco", cartasAzules: 2, cartasVerdes: 0, cartasRosas: 0, cartasGrises: 0),
      Partida(Jugador: "Hector", cartasAzules: 4, cartasVerdes: 0, cartasRosas: 0, cartasGrises: 0)
    ];
    var ronda2 = [
      Partida(Jugador: "Andres", cartasAzules: 3, cartasVerdes: 4, cartasRosas: 0, cartasGrises: 0),
      Partida(Jugador: "Marco", cartasAzules: 1, cartasVerdes: 6, cartasRosas: 0, cartasGrises: 0),
      Partida(Jugador: "Hector", cartasAzules: 1, cartasVerdes: 5, cartasRosas: 0, cartasGrises: 0)
    ];
    var ronda3 = [
      Partida(Jugador: "Andres", cartasAzules: 1, cartasVerdes: 2, cartasRosas: 7, cartasGrises: 3),
      Partida(Jugador: "Marco", cartasAzules: 2, cartasVerdes: 0, cartasRosas: 5, cartasGrises: 7),
      Partida(Jugador: "Hector", cartasAzules: 2, cartasVerdes: 1, cartasRosas: 4, cartasGrises: 4)
    ];

    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules*3,
        id: 'a3',
        data: ronda3,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules*3,
        id: 'a2',
        data: ronda2,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF2D98DA)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules*3,
        id: 'a1',
        data: ronda1,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF33A7FF)),
      ),
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasVerdes*4,
        id: 'v3',
        data: ronda3,
        seriesCategory: '2',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF33FF41)),
      ), 
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasVerdes*4,
        id: 'v2',
        data: ronda2,
        seriesCategory: '2',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF63D533)),
      ),
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasGrises*7,
        id: 'g3',
        data: ronda3,
        seriesCategory: '3',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFFC0C0C0)),
      ),
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => puntosPorRosa[pollution.cartasRosas],
        id: 'r3',
        data: ronda3,
        seriesCategory: '4',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFFFF69B4)),
      ),
    );

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_seriesData =[];
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff1976d2),
            bottom: TabBar(
              indicatorColor: Color(0xff9962D0),
              tabs: [
                Tab(icon: Icon(FontAwesomeIcons.crown)),
                Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
              ],
            ),
            title: Text('Sabado 20 de noviembre, 10:15 pm'),
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text( 'Resultado Final',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        SizedBox(height: 30.0,),
                        Column(
                          children:[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children:[
                                Text( '1ro',style: TextStyle(fontSize: 48.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( 'Marco',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( '142',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                              ],
                            ),
                            SizedBox(height: 10.0,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children:[
                                Text( '2do',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( 'Andres',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( '137',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                              ],
                            ),
                            SizedBox(height: 10.0,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children:[
                                Text( '3ro',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( 'Hector',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                                SizedBox(width: 10.0,),
                                Text( '130',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            //
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'Puntuaciones por ronda',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        Expanded(
                          child: charts.BarChart(
                            _seriesData,
                            animate: true,
                            barGroupingType: charts.BarGroupingType.groupedStacked,
                            animationDuration: Duration(seconds: 0),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Partida {
  String Jugador;
  int cartasAzules;
  int cartasVerdes;
  int cartasRosas;
  int cartasGrises;
  Partida({
    required this.Jugador,
    required this.cartasAzules,
    required this.cartasVerdes,
    required this.cartasRosas,
    required this.cartasGrises,
  });

}

