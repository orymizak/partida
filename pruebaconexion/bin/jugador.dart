import 'dart:convert';

class Jugador {
  final String nombre;
  final int cartasAzules;
  final int cartasVerdes;
  final int cartasRosas;
  final int cartasGrises;
  Jugador({
    required this.nombre,
    required this.cartasAzules,
    required this.cartasVerdes,
    required this.cartasRosas,
    required this.cartasGrises,
  });

  Jugador copyWith({
    String? nombre,
    int? cartasAzules,
    int? cartasVerdes,
    int? cartasRosas,
    int? cartasGrises,
  }) {
    return Jugador(
      nombre: nombre ?? this.nombre,
      cartasAzules: cartasAzules ?? this.cartasAzules,
      cartasVerdes: cartasVerdes ?? this.cartasVerdes,
      cartasRosas: cartasRosas ?? this.cartasRosas,
      cartasGrises: cartasGrises ?? this.cartasGrises,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'cartasAzules': cartasAzules,
      'cartasVerdes': cartasVerdes,
      'cartasRosas': cartasRosas,
      'cartasGrises': cartasGrises,
    };
  }

  factory Jugador.fromMap(Map<String, dynamic> map) {
    return Jugador(
      nombre: map['nombre'],
      cartasAzules: map['cartasAzules'],
      cartasVerdes: map['cartasVerdes'],
      cartasRosas: map['cartasRosas'],
      cartasGrises: map['cartasGrises'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Jugador.fromJson(String source) => Jugador.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Jugador(nombre: $nombre, cartasAzules: $cartasAzules, cartasVerdes: $cartasVerdes, cartasRosas: $cartasRosas, cartasGrises: $cartasGrises)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Jugador &&
      other.nombre == nombre &&
      other.cartasAzules == cartasAzules &&
      other.cartasVerdes == cartasVerdes &&
      other.cartasRosas == cartasRosas &&
      other.cartasGrises == cartasGrises;
  }

  @override
  int get hashCode {
    return nombre.hashCode ^
      cartasAzules.hashCode ^
      cartasVerdes.hashCode ^
      cartasRosas.hashCode ^
      cartasGrises.hashCode;
  }
}
