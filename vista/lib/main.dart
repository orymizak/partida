import 'package:flutter/material.dart';
import 'package:vista/src/bienvenida.dart';
import 'package:vista/src/iniciarpartida.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Bienvenida(),
        routes: <String, WidgetBuilder> {
          '/Inicio': (BuildContext context) => Bienvenida(),
          '/Partida': (BuildContext context) => IniciarPartida(),
          // '/Register': (BuildContext context) => Register(),
          // '/LoggedIn': (BuildContext context) => LoggedIn(),
          // '/Recover': (BuildContext context) => Recover(),
          // '/Account': (BuildContext context) => Account(),
          // '/Data': (BuildContext context) => Data(),
          // '/History': (BuildContext context) => History(),
          // '/Home': (BuildContext context) => Home(),
        });
  }

}
