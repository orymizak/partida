import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vista/src/iniciarpartida.dart';

class Bienvenida extends StatelessWidget {
  const Bienvenida({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
              child: Column(
            children: [ 
              const Text("bienvenida"),
              ElevatedButton(
                  child: const Text("Ingresar"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const IniciarPartida()),
                    );
                  }),
            ],
          )),
        ],
      ),
    );
  }
}
