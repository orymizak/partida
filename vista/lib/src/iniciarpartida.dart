import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vista/src/jugar.dart';

class IniciarPartida extends StatelessWidget {
  const IniciarPartida({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          child: Column(
        children: [
          Text("Nombre jugador 1"),
          TextFormField(),
          Text("Nombre jugador 2"),
          TextFormField(),
          Text("Nombre jugador 3"),
          TextFormField(),
          Text("Nombre jugador 4"),
          TextFormField(),
          ElevatedButton(onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Jugar()),
                    );
          }, child: Text("Jugar")),
        ],
      )),
    );
  }
}
