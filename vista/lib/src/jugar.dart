import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

TextEditingController j1r1 = TextEditingController();
TextEditingController j1r2 = TextEditingController();
TextEditingController j1r3 = TextEditingController();

TextEditingController j2r1 = TextEditingController();
TextEditingController j2r2 = TextEditingController();
TextEditingController j2r3 = TextEditingController();

TextEditingController j3r1 = TextEditingController();
TextEditingController j3r2 = TextEditingController();
TextEditingController j3r3 = TextEditingController();

TextEditingController j4r1 = TextEditingController();
TextEditingController j4r2 = TextEditingController();
TextEditingController j4r3 = TextEditingController();

final FocusNode rutaActual = FocusNode();
final FocusNode rutaNueva = FocusNode();

class Jugar extends StatelessWidget {
  const Jugar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int i = 1;
    return Scaffold(
      appBar: AppBar(),
      body: Row(
        children: [
          Flexible(
            child: Column(
              children: [
                Text("test"),
                TextFormField(
                    textInputAction: TextInputAction.next,
                    focusNode: rutaActual,
                    onFieldSubmitted: (term) {
                      rutaActual.unfocus();
                      FocusScope.of(context).requestFocus(rutaNueva);
                    }),
                TextFormField(
                    textInputAction: TextInputAction.next,
                    focusNode: rutaNueva,
                    onFieldSubmitted: (term) {
                      rutaActual.unfocus();
                      FocusScope.of(context).requestFocus(rutaNueva);
                    }),
                TextFormField(textInputAction: TextInputAction.next),
                TextFormField(textInputAction: TextInputAction.next),
              ],
            ),
          ),
          Flexible(
            child: Column(
              children: [
                Text("test"),
                TextFormField(),
                TextFormField(),
                TextFormField(),
                TextFormField(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _test() {
    return Container(
      child: Text("asd"),
      constraints: BoxConstraints.expand(),
    );
  }

  _hilera(i, context) {
    return TableRow(
      children: [
        SizedBox(child: Text("jugador " + i.toString())),
        _celdas(j1r1, context),
        _celdas(j1r2, context),
        _celdas(j1r3, context),
      ],
    );
  }

  _celdas(puntaje, context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: SizedBox(
        height: 50,
        width: 50,
        child: Flexible(
          child: TextFormField(
            keyboardType: TextInputType.number,
            maxLength: 2,
            controller: puntaje,
            textInputAction: TextInputAction.next,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
            ],
          ),
        ),
      ),
    );
  }
}
